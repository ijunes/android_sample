package com.ijunes.ui

import io.reactivex.rxjava3.subjects.PublishSubject

object NavigationSubject  {
    private var subject: PublishSubject<NavigationEvent>? = null

    fun getSubject(): PublishSubject<NavigationEvent> {
        if (subject == null) {
            subject = PublishSubject.create()
        }
        return subject!!
    }
}