package com.ijunes.ui

sealed class NavigationEvent
data class ListEvent(val id: String?): NavigationEvent()