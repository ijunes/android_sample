package com.ijunes.mvp

/**
 * Base interface for View
 */
interface MVPView {

    fun setProgressVisibility(isVisible: Boolean = true)
    fun setErrorVisibility(isVisible: Boolean = true)

}