package com.ijunes.mvp

/**
 * Base interface for Presenter
 */
interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    var view: V?

}