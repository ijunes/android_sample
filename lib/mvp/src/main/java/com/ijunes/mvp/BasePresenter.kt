package com.ijunes.mvp

import com.ijunes.core.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BasePresenter<V : MVPView, I : MVPInteractor>(protected var interactor: I?, protected val schedulerProvider: SchedulerProvider) : MVPPresenter<V, I> {

    override var view: V? = null
    private val isViewAttached: Boolean get() = view != null
    protected val compositeDisposable = CompositeDisposable()
    protected var initialized = false

    override fun onAttach(view: V?) {
        this.view = view
    }

    override fun onDetach() {
        compositeDisposable.dispose()
        view = null
        interactor = null
    }


}