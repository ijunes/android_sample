package com.ijunes.core

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.schedulers.Schedulers

open class SchedulerProvider {

  open fun <T> ioToMainObservableScheduler(): ObservableTransformer<T, T> = ObservableTransformer { upstream ->
    upstream.subscribeOn(getIOThreadScheduler())
            .observeOn(getMainThreadScheduler())
  }

  open fun <T> ioToMainSingleScheduler(): SingleTransformer<T, T> = SingleTransformer { upstream ->
    upstream.subscribeOn(getIOThreadScheduler())
            .observeOn(getMainThreadScheduler())
  }

  open fun ioToMainCompletableScheduler(): CompletableTransformer = CompletableTransformer { upstream ->
    upstream.subscribeOn(getIOThreadScheduler())
            .observeOn(getMainThreadScheduler())
  }

  open fun <T> ioToMainFlowableScheduler(): FlowableTransformer<T, T> = FlowableTransformer { upstream ->
    upstream.subscribeOn(getIOThreadScheduler())
            .observeOn(getMainThreadScheduler())
  }

  open fun <T> ioToMainMaybeScheduler(): MaybeTransformer<T, T> = MaybeTransformer { upstream ->
    upstream.subscribeOn(getIOThreadScheduler())
            .observeOn(getMainThreadScheduler())
  }

  open fun getIOThreadScheduler() = Schedulers.io()

  open fun getMainThreadScheduler() = AndroidSchedulers.mainThread()

}