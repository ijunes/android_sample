package com.ijunes.core

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class SchedulersModule {

  @Provides
  fun schedulers(): SchedulerProvider = SchedulerProvider()

}