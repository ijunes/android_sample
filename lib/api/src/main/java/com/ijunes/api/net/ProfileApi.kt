package com.ijunes.api.net

import com.ijunes.api.dto.ProfileResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileApi {

    @GET("v2/restaurant/{id}/")
    fun getProfile(@Path("id") id: String?): Single<ProfileResponse>

}