package com.ijunes.api.net

import com.ijunes.api.dto.SearchResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ListApi {

    @GET("v1/store_feed/")
    fun getResults(@Query("lat") latitude: Double,
                   @Query("lng") longitude: Double,
                   @Query("offset") offset: Int,
                   @Query("limit") limit: Int): Single<SearchResponse>

}