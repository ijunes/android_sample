package com.ijunes.api.dto

import com.squareup.moshi.Json

data class SearchItem(
        @Json(name = "id") val id: String?,
        @Json(name = "name") val name: String?,
        @Json(name = "description") val description: String?,
        @Json(name = "cover_img_url") val photoUrl: String?,
        @Json(name = "status") val status: Status?,
)

data class Status (
        @Json(name = "asap_available") val asapAvailable: Boolean?,
        @Json(name = "asap_minutes_range") val asapRanges: List<Int>?,
)