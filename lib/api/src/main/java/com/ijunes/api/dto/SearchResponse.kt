package com.ijunes.api.dto

import com.squareup.moshi.Json

data class SearchResponse(@Json(name = "num_results") val count: Int? = null,
                          @Json(name = "next_offset") val nextOffset: Int? = null,
                          @Json(name = "stores") val stores: List<SearchItem>? = null)

