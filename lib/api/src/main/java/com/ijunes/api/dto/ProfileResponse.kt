package com.ijunes.api.dto

import com.squareup.moshi.Json
import java.util.*

data class ProfileResponse(
        @Json(name = "id") val id: String? = null,
        @Json(name = "business_id") val businessId: String? = null,
        @Json(name = "average_rating") val averageRating: String? = null,
        @Json(name = "num_ratings") val ratingsCount: Int = 0,
        @Json(name = "description") val description: String? = null,
        @Json(name = "cover_img_url") val photoUrl: String? = null,
        @Json(name = "header_img_url") val headerPhotoUrl: String? = null,
        @Json(name = "price_range") val priceRange: Int? = null,
        @Json(name = "is_newly_added") val isNewlyAdded: Boolean = false,
        @Json(name = "url") val url: String? = null,
        @Json(name = "next_open_time") val nextOpenTime: Date? = null,
        @Json(name = "next_clsoe_time") val nextCloseTime: Date? = null,
        @Json(name = "name") val name: String? = null,
        @Json(name = "distance_from_consumer") val distance: Double? = null,
        @Json(name = "menus") val menus: List<MenuGroup>? = null,
        @Json(name = "location") val location: Double? = null,
        @Json(name = "address") val address: Address? = null,
        @Json(name = "phone_number") val phone: String? = null,
        @Json(name = "asap_time") val asapTime: String? = null,
        )

data class MenuGroup(
        @Json(name = "id") val id: String? = null,
        @Json(name = "subtitle") val subtitle: String? = null,
        @Json(name = "name") val name: String? = null,
        @Json(name = "is_catering") val isCatering: Boolean = false,
)

data class Location(
        @Json(name = "lat") val latitude: Double? = null,
        @Json(name = "lng") val longitude: Double? = null,
)

data class Address(
        @Json(name = "printable_address") val displayAddress: String? = null,
        )