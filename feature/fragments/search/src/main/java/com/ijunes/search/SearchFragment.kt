package com.ijunes.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.ijunes.api.dto.SearchItem
import com.ijunes.search.mvp.SearchMVPInteractor
import com.ijunes.search.mvp.SearchMVPPresenter
import com.ijunes.search.mvp.SearchMVPView
import com.ijunes.profile.R
import com.ijunes.ui.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : Fragment(), SearchMVPView {

    companion object {
        val TAG = SearchFragment::class.simpleName
        const val VIEW_CACHE_SIZE = 6
    }

    private val adapter by lazy {
        SearchAdapter().apply { this.registerLifeCycleObserver(this@SearchFragment.lifecycle) }
    }

    @Inject
    internal lateinit var presenter: SearchMVPPresenter<SearchMVPView, SearchMVPInteractor>

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.init(37.422740, -122.139956, adapter.eventStream)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val lm = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.toolbar.title = getString(R.string.discover_title)
        binding.recyclerView.layoutManager = lm
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setItemViewCacheSize(VIEW_CACHE_SIZE)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, lm.getOrientation()))

        binding.refreshLayout.setOnRefreshListener { presenter.retry() }

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                binding.refreshLayout.isEnabled = lm.findFirstCompletelyVisibleItemPosition() == 0
                val visibleItemCount: Int = lm.childCount
                val totalItemCount: Int = lm.itemCount
                val firstVisibleItemPosition: Int = lm.findFirstVisibleItemPosition()
                if (!presenter.isLoading() && totalItemCount < presenter.getTotalCount() && firstVisibleItemPosition >= totalItemCount - visibleItemCount) {
                    presenter.fetchStores()
                }
            }
        })

    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        _binding = null
        super.onDestroyView()
    }

    override fun loadResults(reviewResponse: List<SearchItem>, clearData: Boolean) {
        setErrorVisibility(false)
        adapter.updateItems(reviewResponse, clearData)
    }

    override fun setProgressVisibility(isVisible: Boolean) {
        _binding?.refreshLayout?.isRefreshing = isVisible
    }
    override fun setErrorVisibility(isVisible: Boolean) {
        _binding?.errorRoot?.root?.isVisible = isVisible
    }
}
