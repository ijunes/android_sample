package com.ijunes.search

import com.ijunes.core.SchedulerProvider
import com.ijunes.core.ifLet
import com.ijunes.search.mvp.SearchMVPInteractor
import com.ijunes.search.mvp.SearchMVPPresenter
import com.ijunes.search.mvp.SearchMVPView
import com.ijunes.mvp.BasePresenter
import com.ijunes.ui.ListEvent
import com.ijunes.ui.NavigationSubject
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

class SearchPresenter<V : SearchMVPView, I : SearchMVPInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider), SearchMVPPresenter<V, I> {

    var lat: Double? = null
        private set
    var lng: Double? = null
        private set
    var maxItems: Int = 0
        private set
    var loading: Boolean = false
        private set
    var currentOffset = 0
        private set

    override fun onAttach(view: V?) {
        super.onAttach(view)
        fetchStores()
    }

    override fun fetchStores() {
        if (currentOffset > -1) {
            ifLet(this.lat, this.lng) { (safeLat, safeLng) ->
                loading = true
                view?.setProgressVisibility(true)
                compositeDisposable.add(interactor?.getResults(safeLat, safeLng, currentOffset, PAGE_LIMIT)
                        ?.compose(schedulerProvider.ioToMainSingleScheduler())
                        ?.subscribe({ response ->
                            val clearData = this.currentOffset == 0
                            loading = false
                            this.maxItems = response.count ?: maxItems
                            this.currentOffset = response.nextOffset ?: -1
                            view?.setProgressVisibility(false)
                            response.stores?.map { store ->
                                val newDescription =  store.description?.split(',')
                                        ?.takeIf { it.size > 2 }
                                        ?.let {
                                            "${it[0]},${it[1]}"
                                        } ?: store.description
                                store.copy(description = newDescription)
                            }?.let {
                                view?.loadResults(it, clearData)
                            }
                        }, { err ->
                            loading = false
                            this.currentOffset = -1
                            view?.setProgressVisibility(false)
                            view?.setErrorVisibility()
                        }))
            }
        }
    }

    override fun retry() {
        currentOffset = 0
        view?.setErrorVisibility(false)
        fetchStores()
    }

    override fun init(lat: Double, lng: Double, eventStream: PublishSubject<ListEvent>) {
        if (!initialized) {
            this.initialized = true
            this.lat = lat
            this.lng = lng
            eventStream.compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe { event ->
                        onAdapterEvent(event)
                    }.addTo(compositeDisposable)
        }
    }

    private fun onAdapterEvent(event: ListEvent) {
        NavigationSubject.getSubject().onNext(event)
    }

    companion object {
        const val PAGE_LIMIT = 50
    }

    override fun getTotalCount(): Int {
        return this.maxItems
    }

    override fun isLoading(): Boolean {
        return loading
    }

}