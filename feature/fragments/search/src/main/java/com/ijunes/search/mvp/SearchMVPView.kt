package com.ijunes.search.mvp

import com.ijunes.api.dto.SearchItem
import com.ijunes.mvp.MVPView

interface SearchMVPView : MVPView {

    fun loadResults(reviewResponse: List<SearchItem>, clearData: Boolean)

}