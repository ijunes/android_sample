package com.ijunes.search.view

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.ijunes.api.dto.SearchItem
import com.ijunes.core.GlideApp
import com.ijunes.profile.R
import com.ijunes.ui.ListEvent
import com.ijunes.ui.databinding.SearchItemBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class SearchViewHolder(private val binding: SearchItemBinding, eventStream: PublishSubject<ListEvent>) : RecyclerView.ViewHolder(binding.root) {

    var storeId: String? = null

    init {
        itemView.setOnClickListener { eventStream.onNext(ListEvent(storeId)) }
    }

    fun bind(data: SearchItem) {
        this.storeId = data.id

        GlideApp.with(itemView.context).load(data.photoUrl)
                .centerInside()
                .transition(withCrossFade(150))
                .into(binding.restaurantImage)

        binding.restaurantName.text = data.name
        binding.restaurantCuisine.text = data.description

        val etaText = data.status?.asapRanges?.minOrNull()
                ?.takeIf { data.status?.asapAvailable == true }
                ?.let { lowestRange ->
            itemView.resources.getString(R.string.eta_format, lowestRange)
        } ?: itemView.resources.getString(R.string.closed)
        binding.restaurantEta.text = etaText
    }

}