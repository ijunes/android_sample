package com.ijunes.search

import com.ijunes.search.mvp.SearchMVPInteractor
import com.ijunes.search.mvp.SearchMVPPresenter
import com.ijunes.search.mvp.SearchMVPView
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
class SearchFragmentModule {

    @Provides
    @ActivityScoped
    internal fun provideSearchInteractor(mainInteractor: SearchInteractor): SearchMVPInteractor = mainInteractor

    @Provides
    @ActivityScoped
    internal fun provideSearchPresenter(mainPresenter: SearchPresenter<SearchMVPView, SearchMVPInteractor>)
            : SearchMVPPresenter<SearchMVPView, SearchMVPInteractor> = mainPresenter

}