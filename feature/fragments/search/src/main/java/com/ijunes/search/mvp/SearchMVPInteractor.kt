package com.ijunes.search.mvp

import com.ijunes.api.dto.SearchResponse
import com.ijunes.mvp.MVPInteractor
import io.reactivex.rxjava3.core.Single

interface SearchMVPInteractor : MVPInteractor {

    fun  getResults(latitude: Double, longitude: Double, offset: Int, limit: Int): Single<SearchResponse>

}