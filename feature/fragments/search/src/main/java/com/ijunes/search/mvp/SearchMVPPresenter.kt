package com.ijunes.search.mvp

import com.ijunes.mvp.MVPPresenter
import com.ijunes.ui.ListEvent
import io.reactivex.rxjava3.subjects.PublishSubject

interface SearchMVPPresenter<V : SearchMVPView, I : SearchMVPInteractor> : MVPPresenter<V, I> {

    fun getTotalCount(): Int
    fun isLoading(): Boolean
    fun fetchStores()
    fun retry()
    fun init(lat: Double, lng: Double, eventStream: PublishSubject<ListEvent>)
}