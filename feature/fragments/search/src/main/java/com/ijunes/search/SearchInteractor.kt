package com.ijunes.search

import com.ijunes.api.dto.SearchResponse
import com.ijunes.api.net.ListApi
import com.ijunes.search.mvp.SearchMVPInteractor
import io.reactivex.rxjava3.core.Single
import retrofit2.Retrofit
import javax.inject.Inject

class SearchInteractor @Inject internal constructor(retrofit: Retrofit) : SearchMVPInteractor {

    private val api = retrofit.create(ListApi::class.java)

    override fun getResults(latitude: Double, longitude: Double, offset: Int, limit: Int): Single<SearchResponse> {
        return api.getResults(latitude = latitude, longitude = longitude, offset = offset, limit = limit)
    }

}
