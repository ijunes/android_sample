package com.ijunes.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.RecyclerView
import com.ijunes.api.dto.SearchItem
import com.ijunes.search.view.SearchViewHolder
import com.ijunes.ui.ListEvent
import com.ijunes.ui.databinding.SearchItemBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class SearchAdapter : RecyclerView.Adapter<SearchViewHolder>(), LifecycleObserver {
    var data: ArrayList<SearchItem> = ArrayList()

    val eventStream = PublishSubject.create<ListEvent>()

    fun registerLifeCycleObserver(parentLifecycle: Lifecycle) {
        parentLifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun stopEventStream() {
        eventStream.onComplete()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemBinding = SearchItemBinding.inflate(inflater, parent, false)
        return SearchViewHolder(itemBinding, eventStream)
    }

    override fun getItemCount(): Int =
            data.size


    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(data.get(position))
    }

    fun updateItems(newItems: List<SearchItem>, clear: Boolean = false){
        if(clear){
            data.clear()
        }
        data.addAll(newItems)
        notifyDataSetChanged()
    }

}