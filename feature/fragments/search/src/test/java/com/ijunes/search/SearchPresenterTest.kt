package com.ijunes.search

import com.ijunes.api.dto.SearchResponse
import com.ijunes.core.TestSchedulerProvider
import com.ijunes.search.mvp.SearchMVPInteractor
import com.ijunes.search.mvp.SearchMVPView
import com.ijunes.ui.ListEvent
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test


class SearchPresenterTest {
    lateinit var presenter: SearchPresenter<SearchMVPView, SearchMVPInteractor>

    @MockK
    lateinit var mockInteractor: SearchInteractor

    @MockK
    lateinit var mockView: SearchMVPView

    lateinit var compositeDisposable: CompositeDisposable
    lateinit var testSchedulerProvider: TestSchedulerProvider

    private val eventStream: PublishSubject<ListEvent> = PublishSubject.create()

    @Before
    fun setup(){
        MockKAnnotations.init(this, relaxUnitFun = true)
        compositeDisposable = CompositeDisposable()
        testSchedulerProvider = TestSchedulerProvider()
        presenter = SearchPresenter(interactor = mockInteractor, schedulerProvider = testSchedulerProvider)
    }

    @Test
    fun `empty list from response`() {
        // given
        every { mockInteractor.getResults(any(), any(), any(), any())} returns Single.just(SearchResponse())
        presenter.init(0.0, 0.0, eventStream)
        assertTrue(presenter.lat == 0.0)
        assertTrue(presenter.lng == 0.0)

        // when
        presenter.onAttach(mockView)
        testSchedulerProvider.testScheduler.triggerActions()

        // then
        verify(exactly = 1) {
            mockView.setProgressVisibility()
            mockInteractor.getResults(0.0, 0.0, 0, any())
            mockView.setProgressVisibility(false)
        }
        verify(exactly = 0){
            mockView.loadResults(any(), any())
        }
    }

    @Test
    fun `network error while fetching list`() {
        every { mockInteractor.getResults(any(), any(), any(), any())} returns Single.create { emitter ->
            emitter.onError(Throwable())
        }
        presenter.init(0.0, 0.0, eventStream)
        presenter.onAttach(mockView)
        assertTrue(presenter.isLoading())
        testSchedulerProvider.testScheduler.triggerActions()
        verify(exactly = 1) {
            mockView.setProgressVisibility()
            mockInteractor.getResults(0.0, 0.0, 0, any())
            mockView.setProgressVisibility(false)
            mockView.setErrorVisibility(true)
        }
        verify(exactly = 0){
            mockView.loadResults(any(), any())
        }
    }

    @Test
    fun `verify offsets update`() {
        // given
        val mockResponse = SearchResponse(100, 50, emptyList())
        every { mockInteractor.getResults(any(), any(), any(), any())} returns  Single.just(mockResponse)
        presenter.init(0.0, 0.0, eventStream)
        presenter.onAttach(mockView)
        assertEquals(0, presenter.currentOffset)
        testSchedulerProvider.testScheduler.triggerActions()

        // when
        presenter.fetchStores()
        testSchedulerProvider.testScheduler.triggerActions()

        // then
        verify {
            mockView.setProgressVisibility()
            mockInteractor.getResults(0.0, 0.0, 0, any())
            mockView.setProgressVisibility(false)
            mockView.setProgressVisibility()
            mockInteractor.getResults(0.0, 0.0, 50, any())
            mockView.setProgressVisibility(false)
        }
        verify(exactly = 2){
            mockView.loadResults(any(), any())
        }
    }


    @Test
    fun `verify offsets resets on refresh`() {
        // given
        val mockResponse = SearchResponse(100, 50, emptyList())
        every { mockInteractor.getResults(any(), any(), any(), any())} returns  Single.just(mockResponse)
        presenter.init(0.0, 0.0, eventStream)
        presenter.onAttach(mockView)
        assertEquals(0, presenter.currentOffset)
        testSchedulerProvider.testScheduler.triggerActions()
        presenter.fetchStores()
        testSchedulerProvider.testScheduler.triggerActions()
        assertTrue(presenter.maxItems == 100)

        // when
        presenter.retry()
        assertEquals(0, presenter.currentOffset)
        testSchedulerProvider.testScheduler.triggerActions()

        // then
        verify {
            mockView.setProgressVisibility()
            mockInteractor.getResults(0.0, 0.0, 0, any())
            mockView.setProgressVisibility(false)
            mockView.setProgressVisibility()
            mockInteractor.getResults(0.0, 0.0, 50, any())
            mockView.setProgressVisibility(false)
        }
        verify(exactly = 3){
            mockView.loadResults(any(), any())
        }
    }

}