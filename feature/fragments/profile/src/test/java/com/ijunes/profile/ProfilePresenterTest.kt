package com.ijunes.profile

import com.ijunes.api.dto.ProfileResponse
import com.ijunes.core.TestSchedulerProvider
import com.ijunes.profile.mvp.ProfileMVPInteractor
import com.ijunes.profile.mvp.ProfileMVPView
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.subjects.PublishSubject
import org.junit.Assert
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class ProfilePresenterTest {

    lateinit var presenter: ProfilePresenter<ProfileMVPView, ProfileMVPInteractor>
    @MockK
    lateinit var mockView: ProfileMVPView
    @MockK
    lateinit var mockInteractor: ProfileMVPInteractor
    val testSchedulerProvider = TestSchedulerProvider()
    val TEST_ID = "1"
    val eventStream: PublishSubject<ProfileEvent> = PublishSubject.create()
    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        presenter = ProfilePresenter(schedulerProvider = testSchedulerProvider, interactor = mockInteractor)
    }

    @Test
    fun `test valid profile`() {
        // given
        val mockResponse = ProfileResponse(photoUrl = "photoUrl", name = "name", asapTime = "38", averageRating = "5")
        presenter.init(TEST_ID, eventStream)
        every { mockInteractor.getProfile(TEST_ID) } returns Single.just(mockResponse)
        val slot = slot<List<ListItem>>()
        every { mockView.showData(capture(slot)) }
        presenter.onAttach(mockView)

        // when
        testSchedulerProvider.testScheduler.triggerActions()

        // then
        assertTrue(slot.captured.isNotEmpty())
        assertTrue(slot.captured[0] is Header)
        assertTrue(slot.captured[1] is Info)
        assertTrue(slot.captured[2] is Ratings)
        assertTrue(slot.captured[3] is Status)
    }

    @Test
    fun addition_isCorrect() {
        Assert.assertEquals(4, 2 + 2.toLong())
    }
}