package com.ijunes.profile.view

import androidx.recyclerview.widget.RecyclerView
import com.ijunes.profile.ProfileEvent
import com.ijunes.profile.SuggestedItems
import com.ijunes.ui.databinding.ProfileItemSuggestedItemsBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class SuggestedItemsViewHolder(private val binding: ProfileItemSuggestedItemsBinding, eventStream: PublishSubject<ProfileEvent>): RecyclerView.ViewHolder(binding.root) {
    fun bind(data: SuggestedItems) {
    }

}