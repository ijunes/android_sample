package com.ijunes.profile

import EXTRA_RESTAURANT_ID
import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager

import com.ijunes.profile.mvp.ProfileMVPInteractor
import com.ijunes.profile.mvp.ProfileMVPPresenter
import com.ijunes.profile.mvp.ProfileMVPView
import com.ijunes.ui.CommonUtil
import com.ijunes.ui.databinding.FragmentProfileBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment(), ProfileMVPView {

    private var progressDialog: ProgressDialog? = null

    companion object {
        fun createInstance(id: String?): ProfileFragment {
            return ProfileFragment().apply {
                this.arguments = Bundle().apply {
                    putString(EXTRA_RESTAURANT_ID, id)
                }
            }
        }

        val TAG = ProfileFragment::class.simpleName
    }

    val adapter by lazy {
        ProfileAdapter().apply { this.registerLifeCycleObserver(this@ProfileFragment.lifecycle) }
    }

    @Inject
    internal lateinit var presenter: ProfileMVPPresenter<ProfileMVPView, ProfileMVPInteractor>
    var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = arguments?.getString(EXTRA_RESTAURANT_ID)
        presenter.init(id, adapter.eventStream)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        _binding = null
        super.onDestroyView()
    }

    override fun showData(data: List<ListItem>) {
        adapter.data = data
    }

    override fun showClickToast(event: String?) {
        context?.let {
            Toast.makeText(it, event, Toast.LENGTH_SHORT).show()
        }
    }

    override fun setErrorVisibility(isVisible: Boolean) {
        binding.errorRoot.root.isVisible = isVisible
    }

    override fun setProgressVisibility(isVisible: Boolean) {
        hideProgress()
        if (isVisible) {
            progressDialog = CommonUtil.showLoadingDialog(context)
        }
    }

    private fun hideProgress() {
        progressDialog?.let { if (it.isShowing) it.cancel() }
        progressDialog = null
    }
}
