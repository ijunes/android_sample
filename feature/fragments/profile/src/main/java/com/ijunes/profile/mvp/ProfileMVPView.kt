package com.ijunes.profile.mvp

import com.ijunes.mvp.MVPView
import com.ijunes.profile.ListItem

interface ProfileMVPView : MVPView {

    fun showData(data: List<ListItem>)
    fun showClickToast(event: String?)

}