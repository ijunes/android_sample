package com.ijunes.profile.view

import androidx.recyclerview.widget.RecyclerView
import com.ijunes.profile.ASAP
import com.ijunes.profile.ProfileEvent
import com.ijunes.profile.R
import com.ijunes.profile.Status
import com.ijunes.ui.databinding.ProfileItemStatusBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class StatusViewHolder(private val binding: ProfileItemStatusBinding, eventStream: PublishSubject<ProfileEvent>) : RecyclerView.ViewHolder(binding.root) {

    private var displayText: String? = null

    init {
        binding.profileItemStatus.setOnClickListener { eventStream.onNext(ASAP(displayText)) }
    }

    fun bind(data: Status) {
        displayText = itemView.resources.getString(R.string.asap_time_fmt, data.displayText)
        binding.profileItemStatus.text = displayText
    }

}