package com.ijunes.profile

import com.ijunes.api.dto.ProfileResponse
import com.ijunes.core.SchedulerProvider
import com.ijunes.mvp.BasePresenter
import com.ijunes.profile.mvp.ProfileMVPInteractor
import com.ijunes.profile.mvp.ProfileMVPPresenter
import com.ijunes.profile.mvp.ProfileMVPView
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

class ProfilePresenter<V : ProfileMVPView, I : ProfileMVPInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider)
    : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider), ProfileMVPPresenter<V, I> {

    var restaurantId: String? = null
        private set

    override fun onAttach(view: V?) {
        super.onAttach(view)
        fetchRestaurant()
    }

    override fun fetchRestaurant() {
        view?.setErrorVisibility(false)
        view?.setProgressVisibility()
        interactor?.getProfile(restaurantId)
                ?.compose(schedulerProvider.ioToMainSingleScheduler())?.subscribe(
                        { response ->
                            view?.setProgressVisibility(false)
                            generateList(response)
                        },
                        { e ->
                            view?.setProgressVisibility(false)
                            view?.setErrorVisibility(true)
                        })?.let {
                    compositeDisposable.add(it)
                }
    }


    override fun init(id: String?, eventStream: PublishSubject<ProfileEvent>) {
        if (!initialized) {
            this.restaurantId = id
            initialized = true
        }

        // Central location in Presenter to handle Adapter events
        eventStream.compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe { event ->
                    onAdapterEvent(event)
                }.addTo(compositeDisposable)
    }

    private fun onAdapterEvent(event: ProfileEvent) {
        val displayText = when (event) {
            is Photo -> "Photo Clicked"
            is ASAP -> "ASAP Time Clicked"
            is Location -> event.address
        }
        view?.showClickToast(displayText)
    }

    private fun generateList(restaurant: ProfileResponse) {
        val result = mutableListOf<ListItem>()


        // Add Restaurant Header
        (restaurant.headerPhotoUrl ?: restaurant.photoUrl)?.let {
            result.add(Header(headerPhotoUrl = it))
        }

        // Add Info Section
        result.add(Info(
                name = restaurant.name,
                address = restaurant.address?.displayAddress,
                description = restaurant.description,
                phone = restaurant.phone)
        )

        // Add Ratings Section
        if (!restaurant.averageRating.isNullOrEmpty()) {
            result.add(Ratings(restaurant.averageRating, restaurant.ratingsCount.toString()))
        }

        // Add ASAP Section
        restaurant.asapTime?.let {
            result.add(Status(it))
        }

        result.takeIf { it.isNotEmpty() }?.let {
            view?.showData(it)
        } ?: view?.setErrorVisibility(true)
    }
}