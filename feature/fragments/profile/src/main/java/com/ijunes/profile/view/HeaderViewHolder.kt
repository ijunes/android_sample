package com.ijunes.profile.view

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.ijunes.core.GlideApp
import com.ijunes.profile.Header
import com.ijunes.profile.Photo
import com.ijunes.profile.ProfileEvent
import com.ijunes.ui.databinding.ProfileItemHeaderBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class HeaderViewHolder(private val binding: ProfileItemHeaderBinding, eventStream: PublishSubject<ProfileEvent>) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.profileItemHeaderPhoto.setOnClickListener {
            eventStream.onNext(Photo)
        }
    }

    fun bind(data: Header) {
        GlideApp.with(itemView.context)
                .load(data.headerPhotoUrl)
                .centerInside()
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .into(binding.profileItemHeaderPhoto)
    }

}