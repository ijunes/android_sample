package com.ijunes.profile


import com.ijunes.profile.mvp.ProfileMVPInteractor
import com.ijunes.profile.mvp.ProfileMVPPresenter
import com.ijunes.profile.mvp.ProfileMVPView
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class ProfileFragmentModule {

    @Provides
    internal fun provideProfileInteractor(profileInteractor: ProfileInteractor): ProfileMVPInteractor = profileInteractor

    @Provides
    internal fun provideProfilePresenter(mainPresenter: ProfilePresenter<ProfileMVPView, ProfileMVPInteractor>)
            : ProfileMVPPresenter<ProfileMVPView, ProfileMVPInteractor> = mainPresenter

}