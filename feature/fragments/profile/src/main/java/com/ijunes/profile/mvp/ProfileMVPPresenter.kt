package com.ijunes.profile.mvp

import com.ijunes.mvp.MVPPresenter
import com.ijunes.profile.ProfileEvent
import io.reactivex.rxjava3.subjects.PublishSubject


interface ProfileMVPPresenter<V : ProfileMVPView, I : ProfileMVPInteractor> : MVPPresenter<V, I> {

    fun fetchRestaurant()
    fun init(id: String?, eventStream: PublishSubject<ProfileEvent>)
}