package com.ijunes.profile.view

import androidx.recyclerview.widget.RecyclerView
import com.ijunes.profile.ProfileEvent
import com.ijunes.profile.Ratings
import com.ijunes.ui.databinding.ProfileItemRatingBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class RatingsViewHolder(private val binding: ProfileItemRatingBinding, eventStream: PublishSubject<ProfileEvent>) : RecyclerView.ViewHolder(binding.root) {

    fun bind(data: Ratings) {
        binding.profileItemRatingAverageValue.text = data.average
        binding.profileItemRatingCountValue.text = data.count
    }

}