package com.ijunes.profile

sealed class ProfileEvent
object Photo: ProfileEvent()
class Location(val address: String?): ProfileEvent()
class ASAP(val asapTime: String?): ProfileEvent()