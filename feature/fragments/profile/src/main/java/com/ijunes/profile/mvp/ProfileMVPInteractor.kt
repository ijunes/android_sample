package com.ijunes.profile.mvp

import com.ijunes.api.dto.ProfileResponse
import io.reactivex.rxjava3.core.Single

interface ProfileMVPInteractor : com.ijunes.mvp.MVPInteractor {

    fun getProfile(restaurantId: String?): Single<ProfileResponse>

}