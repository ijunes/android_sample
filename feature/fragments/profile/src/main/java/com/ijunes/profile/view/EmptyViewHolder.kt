package com.ijunes.profile.view

import androidx.recyclerview.widget.RecyclerView
import com.ijunes.ui.databinding.EmptyViewBinding

class EmptyViewHolder (binding: EmptyViewBinding): RecyclerView.ViewHolder(binding.root)