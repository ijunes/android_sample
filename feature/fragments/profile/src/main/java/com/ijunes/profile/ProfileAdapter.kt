package com.ijunes.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.RecyclerView
import com.ijunes.profile.ProfileAdapter.Companion.VIEW_TYPE_HEADER
import com.ijunes.profile.ProfileAdapter.Companion.VIEW_TYPE_INFO
import com.ijunes.profile.ProfileAdapter.Companion.VIEW_TYPE_RATING
import com.ijunes.profile.ProfileAdapter.Companion.VIEW_TYPE_STATUS
import com.ijunes.profile.ProfileAdapter.Companion.VIEW_TYPE_SUGGESTED_ITEMS
import com.ijunes.profile.view.*
import com.ijunes.ui.databinding.*
import io.reactivex.rxjava3.subjects.PublishSubject

class ProfileAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(), LifecycleObserver {

    var data: List<ListItem> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    val eventStream: PublishSubject<ProfileEvent> = PublishSubject.create()

    fun registerLifeCycleObserver(parentLifecycle: Lifecycle) {
        parentLifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun stopEventStream() {
        eventStream.onComplete()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_HEADER -> {
                HeaderViewHolder(ProfileItemHeaderBinding.inflate(inflater, parent, false), eventStream)
            }
            VIEW_TYPE_INFO -> {
                InfoViewHolder(ProfileItemInfoBinding.inflate(inflater, parent, false), eventStream)
            }
            VIEW_TYPE_RATING -> {
                RatingsViewHolder(ProfileItemRatingBinding.inflate(inflater, parent, false), eventStream)
            }
            VIEW_TYPE_STATUS -> {
                StatusViewHolder(ProfileItemStatusBinding.inflate(inflater, parent, false), eventStream)
            }
            VIEW_TYPE_SUGGESTED_ITEMS -> {
                SuggestedItemsViewHolder(ProfileItemSuggestedItemsBinding.inflate(inflater, parent, false), eventStream)
            }
            else -> {
                EmptyViewHolder(EmptyViewBinding.inflate(inflater, parent, false))
            }
        }

    }

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int {
        return data[position].viewType
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val currentItem = data.getOrNull(position)) {
            is Header -> {
                (holder as? HeaderViewHolder)?.bind(currentItem)
            }
            is Info -> {
                (holder as? InfoViewHolder)?.bind(currentItem)
            }
            is Ratings -> {
                (holder as? RatingsViewHolder)?.bind(currentItem)
            }
            is Status -> {
                (holder as? StatusViewHolder)?.bind(currentItem)
            }
            is SuggestedItems -> {
                (holder as? SuggestedItemsViewHolder)?.bind(currentItem)
            }
        }
    }

    companion object {
        const val VIEW_TYPE_HEADER = 0
        const val VIEW_TYPE_INFO = 1
        const val VIEW_TYPE_RATING = 2
        const val VIEW_TYPE_STATUS = 3
        const val VIEW_TYPE_SUGGESTED_ITEMS = 4
    }
}

sealed class ListItem(val viewType: Int)
class Header(val headerPhotoUrl: String) : ListItem(VIEW_TYPE_HEADER)
class Info(val name: String?, val address: String?, val description: String?, val phone: String?) : ListItem(VIEW_TYPE_INFO)
class Ratings(val average: String?, val count: String?) : ListItem(VIEW_TYPE_RATING)
class Status(val displayText: String?) : ListItem(VIEW_TYPE_STATUS)
class SuggestedItems() : ListItem(VIEW_TYPE_SUGGESTED_ITEMS)