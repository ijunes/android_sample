package com.ijunes.profile

import com.ijunes.api.dto.ProfileResponse
import com.ijunes.api.net.ProfileApi
import com.ijunes.profile.mvp.ProfileMVPInteractor
import io.reactivex.rxjava3.core.Single
import retrofit2.Retrofit
import javax.inject.Inject

class ProfileInteractor @Inject internal constructor(retrofit: Retrofit) : ProfileMVPInteractor {

    // Retrofit interface
    var api = retrofit.create(ProfileApi::class.java)

    override fun getProfile(restaurantId: String?): Single<ProfileResponse> {
      return api.getProfile(id = restaurantId)
    }

}
