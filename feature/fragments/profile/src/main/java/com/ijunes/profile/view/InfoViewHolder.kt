package com.ijunes.profile.view

import androidx.recyclerview.widget.RecyclerView
import com.ijunes.profile.Info
import com.ijunes.profile.Location
import com.ijunes.profile.ProfileEvent
import com.ijunes.ui.databinding.ProfileItemInfoBinding
import io.reactivex.rxjava3.subjects.PublishSubject

class InfoViewHolder(private val binding: ProfileItemInfoBinding, eventStream: PublishSubject<ProfileEvent>) : RecyclerView.ViewHolder(binding.root){

    var address: String? = null

    init {
        binding.profileItemAddress.setOnClickListener {
            eventStream.onNext(Location(address))
        }
    }

    fun bind(data: Info) {
        this.address = data.address
        binding.profileItemName.text = data.name
        binding.profileItemAddress.text = data.address
        binding.profileItemDescription.text = data.description
    }

}