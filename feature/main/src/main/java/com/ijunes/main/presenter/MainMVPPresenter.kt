package com.ijunes.main.presenter

import com.ijunes.main.MainMVPView
import com.ijunes.main.interactor.MainMVPInteractor
import com.ijunes.mvp.MVPPresenter

interface MainMVPPresenter<V : MainMVPView, I : MainMVPInteractor> : MVPPresenter<V, I> {

}