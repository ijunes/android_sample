package com.ijunes.main

import com.ijunes.main.interactor.MainInteractor
import com.ijunes.main.interactor.MainMVPInteractor
import com.ijunes.main.presenter.MainMVPPresenter
import com.ijunes.main.presenter.MainPresenter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import io.reactivex.rxjava3.disposables.CompositeDisposable

@Module
@InstallIn(ActivityComponent::class)
class MainActivityModule {

    @Provides
    internal fun provideMainInteractor(mainInteractor: MainInteractor): MainMVPInteractor = mainInteractor

    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenter<MainMVPView, MainMVPInteractor>)
            : MainMVPPresenter<MainMVPView, MainMVPInteractor> = mainPresenter

    @Provides
    internal fun provideDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

}