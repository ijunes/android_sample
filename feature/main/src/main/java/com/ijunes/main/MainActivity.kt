package com.ijunes.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.ijunes.search.SearchFragment
import com.ijunes.main.interactor.MainMVPInteractor
import com.ijunes.main.presenter.MainMVPPresenter
import com.ijunes.profile.ProfileFragment
import com.ijunes.ui.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), MainMVPView {

    companion object {
        val TAG = MainActivity::class.simpleName
    }

    @Inject
    internal lateinit var presenter: MainMVPPresenter<MainMVPView, MainMVPInteractor>
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)

        navigateTo(supportFragmentManager.findFragmentByTag(SearchFragment.TAG) ?: SearchFragment(), SearchFragment.TAG)
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onBackPressed() {
        val backStackEntryCount = supportFragmentManager.backStackEntryCount
        if (backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun startProfile(id: String?) {
        val fragment = ProfileFragment.createInstance(id)
        navigateTo(fragment, ProfileFragment.TAG)
    }

    override fun setProgressVisibility(isVisible: Boolean) {
        // no-op in this context
    }

    override fun setErrorVisibility(isVisible: Boolean) {
        // no-op in this context
    }

    private fun navigateTo(fragment: Fragment, tag: String?) {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            addToBackStack(TAG)
            replace(
                    binding.rootContainer.id,
                    fragment,
                    tag
            )
        }
    }

}
