package com.ijunes.main

import com.ijunes.mvp.MVPView

interface MainMVPView : MVPView {
    fun startProfile(id: String?)
}