package com.ijunes.main.presenter

import com.ijunes.core.SchedulerProvider
import com.ijunes.main.MainMVPView
import com.ijunes.main.interactor.MainMVPInteractor
import com.ijunes.mvp.BasePresenter
import com.ijunes.ui.ListEvent
import com.ijunes.ui.NavigationSubject
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenter<V : MainMVPView, I : MainMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider), MainMVPPresenter<V, I> {

    private val navigationDisposable = CompositeDisposable()

    override fun onAttach(view: V?) {
        super.onAttach(view)
        navigationDisposable.add(
                NavigationSubject.getSubject().compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe {
                    when (it) {
                        is ListEvent -> {
                            view?.startProfile(it.id)
                        }
                    }
                })
    }

    override fun onDetach() {
        navigationDisposable.dispose()
        super.onDetach()
    }

}
